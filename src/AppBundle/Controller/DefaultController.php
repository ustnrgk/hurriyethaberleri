<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    const MEMCACHE_ARTICLES_KEY = 'articles';
    const MEMCACHE_COLUMNS_KEY = 'columns';
    const MEMCACHE_CATEGORY_PAGE_KEY_PREFIX = 'articles_';
    const MEMCACHE_ARTICLE_PAGE_KEY_PREFIX = 'article_';
    const MEMCACHE_COLUMN_PAGE_KEY_PREFIX = 'column_';

    /**
     * @Route("/", name="homepage")
     * @return array
     */
    public function indexAction()
    {
        $memcache = $this->get('memcache.default');
        $articles = $memcache->get('articles');
        $columns = $memcache->get('columns');

        if (empty($articles)) {
            $homepageService = $this->get('home_page_service');
            $articles = $homepageService->getHomepagePartByPath('/articles');
            $memcache->set(self::MEMCACHE_ARTICLES_KEY, $articles, 0, 3600);
        }

        if (empty($columns)) {
            $homepageService = $this->get('home_page_service');
            $columns = $homepageService->getHomepagePartByPath('/columns');
            $memcache->set(self::MEMCACHE_COLUMNS_KEY, $columns, 0, 3600);
        }

        return $this->render('hurriyet/index.html.twig', [
            'response' => [
                'articles' => $articles,
                'columns' => $columns
            ]
        ]);
    }

    /**
     * @param string $category
     * @Route("/{category}", name="category", requirements={"category": "[a-zA-Z]+"})
     * @return array
     */
    public function categoryAction($category)
    {
        $memcache = $this->get('memcache.default');
        $categoryArticles = $memcache->get(self::MEMCACHE_CATEGORY_PAGE_KEY_PREFIX . $category);

        if (empty($categoryArticles)) {
            $httpService = $this->get('http_service');
            $categoryArticles = $httpService->apiGet('/articles', [
                'category' => '/' . $category . '/'
            ]);
            $memcache->set('articles_' . $category, $categoryArticles, 0, 3600);
        }

        return $this->render(
            'hurriyet/index.html.twig', [
            'response' => [
                'articles' => $categoryArticles
            ]
        ]);
    }

    /**
     * @param $slug
     * @Route("/{slug}", name="news", requirements={"slug": "[a-zA-Z0-9\-]+"})
     * @return array
     */
    public function newsAction($slug) {
        $slugArray = explode('-', $slug);

        if (!isset($slugArray[count($slugArray) - 1]) || !is_numeric($slugArray[count($slugArray) - 1])) {
            return $this->redirectToRoute('homepage');
        }

        $articleId = $slugArray[count($slugArray) - 1];
        $memcache = $this->get('memcache.default');
        $article = $memcache->get(self::MEMCACHE_ARTICLE_PAGE_KEY_PREFIX . $articleId);

        if (empty($article)) {
            $httpService = $this->get('http_service');
            $article = $httpService->apiGet('/articles/' . $articleId);
            $memcache->set('article_' . $articleId, $article, 0, 3600);
        }

        $article['Text'] = str_replace('<img src="/images', '<img src="http://i.hurimg.com/i/hurriyet', $article['Text']);

        return $this->render(':hurriyet:article.html.twig', [
            'response' => $article
        ]);
    }

    /**
     * @param $slug
     * @param $writer
     * @Route("/yazar/{writer}/{slug}", name="column", requirements={"writer": "[a-zA-Z0-9_\-]+", "slug": "[a-zA-Z0-9_\-]+"})
     * @return array
     */
    public function columnAction($writer, $slug) {
        $slugArray = explode('_', $slug);

        if (!isset($slugArray[count($slugArray) - 1]) || !is_numeric($slugArray[count($slugArray) - 1])) {
            return $this->redirectToRoute('homepage');
        }

        $columnId = $slugArray[count($slugArray) - 1];
        $memcache = $this->get('memcache.default');
        $column = $memcache->get(self::MEMCACHE_COLUMN_PAGE_KEY_PREFIX . $columnId);

        if (empty($column)) {
            $httpService = $this->get('http_service');
            $column = $httpService->apiGet('/columns/' . $columnId);
            $memcache->set('column_' . $columnId, $column, 0, 3600);
        }

        $column['Text'] = str_replace('<img src="/images', '<img src="http://i.hurimg.com/i/hurriyet', $column['Text']);

        return $this->render(':hurriyet:article.html.twig', [
            'response' => $column
        ]);
    }
}
