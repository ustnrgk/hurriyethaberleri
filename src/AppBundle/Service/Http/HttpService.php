<?php

namespace AppBundle\Service\Http;

use AppBundle\Utility\Constants;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

/**
 * Created by PhpStorm.
 * User: gokhanustuner
 * Date: 9/20/16
 * Time: 8:39 PM
 */
class HttpService
{
    private $requestPath;

    private $requestMethod;

    public function apiGet($requestPath, array $params = array()) {
        $this->setRequestPath($requestPath);
        $this->setRequestMethod(Constants::REQUEST_METHOD_GET);
        $requestParams = array(
            'headers' => Constants::$HURRIYET_API_HEADERS,
            'http_errors' => true
        );

        if (count($params) > 0 && isset($params['category'])) {
            $requestParams['query'] = array('$filter' => 'Path eq \''. $params['category'] . '\'');
        }

        $client = new Client();

        try {
            $response = $client->request(
                $this->getRequestMethod(),
                Constants::HURRIYET_API_BASE_REQUEST_URL . $this->getRequestPath(),
                $requestParams
            );
        } catch (ServerException $e) {
            $response = $this->apiGet($this->getRequestPath());
        }

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @return mixed
     */
    private function getRequestPath()
    {
        return $this->requestPath;
    }

    /**
     * @param mixed $requestPath
     */
    private function setRequestPath($requestPath)
    {
        $this->requestPath = $requestPath;
    }

    /**
     * @return mixed
     */
    private function getRequestMethod()
    {
        return $this->requestMethod;
    }

    /**
     * @param mixed $requestMethod
     */
    private function setRequestMethod($requestMethod)
    {
        $this->requestMethod = $requestMethod;
    }
}