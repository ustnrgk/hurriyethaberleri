<?php

namespace AppBundle\Service\Homepage;

use AppBundle\Service\Http\HttpService;

class HomePageService
{
    public function getHomepagePartByPath($path, array $params = array()) {
        $httpService = new HttpService();
        $data = $httpService->apiGet($path, $params);

        return $data;
    }
}