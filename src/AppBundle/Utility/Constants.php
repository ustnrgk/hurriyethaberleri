<?php

namespace AppBundle\Utility;

/**
 * Created by PhpStorm.
 * User: gokhanustuner
 * Date: 9/20/16
 * Time: 8:48 PM
 */
class Constants
{
    const HURRIYET_API_URL = 'https://api.hurriyet.com.tr/';
    const HURRIYET_API_VERSION = 'v1';
    const HURRIYET_API_BASE_REQUEST_URL = 'https://api.hurriyet.com.tr/v1';

    public static $HURRIYET_API_HEADERS = array(
        'accept' => 'application/json',
        'apikey' => '424a899cab5c494ca406d0d7e730eb50'
    );

    const REQUEST_METHOD_GET = 'GET';
    const REQUEST_METHOD_POST = 'POST';
}