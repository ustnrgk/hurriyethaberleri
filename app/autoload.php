<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';
$loader->add('Utility', realpath(__DIR__.'/../src/AppBundle/Utility'));
AnnotationRegistry::registerLoader([$loader, 'loadClass']);

return $loader;
